# Proximity Location iOS SDK Swift Package

## Installing from Xcode

Add a package by selecting File → Add Package Dependencies… in Xcode’s menu bar.

![Add package](./images/add-package.png)

Search for the Proxi Cloud SDK using the repo's URL:

```console
https://gitlab.com/proxi.cloud/proximity-location-ios-sdk-swift-package
```

![alt text](./images/add-package-search.png)

and add to project:

![add-to-project](./images/add-package-assign.png)

# Usage


### Import
```swift
    import ProxiCloud
```

### Initialization
```swift

let proxiCloud: ProxiCloud

init() {
        proxiCloud = ProxiCloud()
        proxiCloud.delegate = self
}
```

### Delegate methods

```swift
    // this method is being calls right after geofences are loaded
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, didLoadGeofences geofences: [ProxiCloudSDK.PCGeofenceProtocol])

    // if the core location authorization status is changed you wil be notified via this method
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, didChangeAuthorisationStatus status: CLAuthorizationStatus)

    // after reporting the event this method will be fired
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, didReportEvent event: ProxiCloudSDK.PCEventProtocol)

    // here you can handle any error provided by ProxiCLoud
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, didRiseAnError error: Error)

    // here you can present view controller provided by InApp action
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, wantsToPresentViewController vc: UIViewController)

    // here you can present URL provided by Deeplink or Website action
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, wantsToPresentURL url: URL)
```

### Setting API key

```swift
    func setApiKey(_ apiKey: String) async throws {
        try await proxiCloud.setApiKey(apiKey)
    }
```

### Setting Advertising Identifier
```swift
    func setAdvertisingIdentifier(_ aid: String) async throws {
        try await proxiCloud.setAdvertisingIdentifier(aid)
    }
```


## Location

### Start monitoring

```swift
    proxiCloud.startMonitoring()
```

### Stop monitoring
```swift
    proxiCloud.stopMonitoring()
```

### Location authorization status
```swift
   let locationAuthorizationStatus: CLAuthorizationStatus = proxiCloud.locationAuthorizationStatus
```

### Geofences

After you start your monitoring and proxi will get your current location, it will autmatically download goefences near your location.

You will be noticed about the list of downloaded objects by delegate method

```swift
    // this method is being calls right after geofences are loaded
    func proxiCloud(_ pc: ProxiCloudSDK.ProxiCloud, didLoadGeofences geofences: [ProxiCloudSDK.PCGeofenceProtocol])
```

you can also access to those objects direct from ProxiCloud instance

```swift
let geofences: [ProxiCloudSDK.PCGeofenceProtocol] = proxiCloud.geofences
```

## Notifications

### Check if can receive notifications

```swift
    let canReceive: Bool = try await proxiCloud.canReceiveNotifications()
```

### Request Notifications Authorization

```swift 
    let granted: Bool = try await proxiCloud.requestNotificationsAuthorization()
```

### Handling notifications

If you want ProxiCloud to handle your notification you can use below method

```swift
extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(
        _ center: UNUserNotificationCenter, 
        willPresent notification: UNNotification, 
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
            
            proxiCloud.handleNotification(notification)
            
            completionHandler([.banner, .sound])
    }
}
```

## Events reporting

First create a class/struct that conforms to `PCCustomEventProtocol`
```swift
struct Event: PCCustomEventProtocol {
    var key: String
    var lifecycle: Int32
    var location: ProxiCloudSDK.PCLocation?
    var locationPermission: Int32
    var value: String
}
```
Then you can report event object 
```swift
func reportCustomEvent() async throws {

    let event = Event(
        key: "key",
        lifecycle: 0,
        locationPermission: 0,
        value: "value"
    )

    try await proxiCloud.reportCustomEvent(event: event)
}
```
