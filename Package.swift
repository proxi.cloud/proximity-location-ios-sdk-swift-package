// swift-tools-version:5.3
import PackageDescription

let package = Package(
    name: "ProxiCloudSDK",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "ProxiCloudSDK",
            targets: ["ProxiCloudSDKPackage","ProxiCloudSDK"]
        ),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/Gracjan12/proximity-location-shared-sdk", .exact("0.0.37")),
        .package(url: "https://github.com/nh7a/Geohash", from: "1.0.0")
    ],
    targets: [
        .target(
            name: "ProxiCloudSDKPackage",
            dependencies: [
                .product(name: "ProximityLocationSharedSDK", package: "proximity-location-shared-sdk"),
                .product(name: "Geohash", package: "Geohash")
            ]
        ),
        .binaryTarget(
            name: "ProxiCloudSDK",
            path: "XCFramework/ProxiCloudSDK.xcframework"
        )
    ]
)
