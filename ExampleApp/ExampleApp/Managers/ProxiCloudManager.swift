//
//  ProxiCloudManager.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 26/08/2022.
//

import Foundation
import ProxiCloudSDK
import CoreLocation
import UIKit
import AppTrackingTransparency

enum ProxiCloudManagerNotification {
    static let didRegisterNewEvent = Notification.Name("ProxiCloudManager.didRegisterNewEvent")
    static let didChangeAuthorisationStatus = Notification.Name("ProxiCloudManager.didChangeAuthorisationStatus")
    static let didLoadGeofences = Notification.Name("ProxiCloudManager.didLoadGeofences")
}

protocol ProxiCloudManagerProtocol {
    var geofences: [PCGeofenceProtocol] { get }
    var reportedEvents: [PCEventProtocol] { get }
    var locationAuthorizationStatus: CLAuthorizationStatus { get }

    var currentAPIKey: String { get }

    func canReceiveNotifications(_ completion:@escaping (Result<Bool, Error>) -> Void)
    func requestNotificationsAuthorization(_ completion:@escaping (Result<Bool, Error>) -> Void)
    func requestLocationAuthorization()

    func setAdIdentifier(_ aid: String) async throws

    func start()
    func handleNotification(_ notification: UNNotification)
}

class ProxiCloudManager: ProxiCloudManagerProtocol {
    static let shared = ProxiCloudManager()

    private let proxiCloud: ProxiCloud
    private let eventsRepository: EventsRepositoryProtocol

    var currentAPIKey: String = ""

    var geofences: [PCGeofenceProtocol] {
        proxiCloud.geofences
    }

    var reportedEvents: [PCEventProtocol] {
        eventsRepository.reportedEvents
    }

    var locationAuthorizationStatus: CLAuthorizationStatus {
        proxiCloud.locationAuthorizationStatus
    }

    init(eventsRepository: EventsRepositoryProtocol = EventsRepository.shared) {
        self.eventsRepository = eventsRepository

        proxiCloud = ProxiCloud()
        proxiCloud.delegate = self
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        proxiCloud.applicationWillEnterForeground(application)
    }

    func canReceiveNotifications(_ completion: @escaping (Result<Bool, Error>) -> Void) {
        proxiCloud.canReceiveNotifications(completion)
    }

    func requestNotificationsAuthorization(_ completion: @escaping (Result<Bool, Error>) -> Void) {
        proxiCloud.requestNotificationsAuthorization(completion)
    }

    func requestLocationAuthorization() {
        proxiCloud.requestLocationAuthorization(always: true)
    }

    func handleNotification(_ notification: UNNotification) {
        proxiCloud.handleNotification(notification)
    }

    func start() {
        proxiCloud.startMonitoring()
    }

    func setAdIdentifier(_ aid: String) async throws {
        try await proxiCloud.setAdvertisingIdentifier(aid)
    }

    func setApiKey(_ apiKey: String) async throws {
        currentAPIKey = apiKey
        try await proxiCloud.setApiKey(apiKey)
    }

    func shouldShowTutorial() -> Bool {
        locationAuthorizationStatus != .authorizedAlways || ATTrackingManager.trackingAuthorizationStatus != .authorized
    }

}

extension ProxiCloudManager: ProxiCloudDelegate {
    func proxiCloud(_ pc: ProxiCloud, wantsToPresentViewController vc: UIViewController) {
        var topViewController = UIApplication.shared.topViewController
        topViewController?.present(vc, animated: true)
    }

    func proxiCloud(_ pc: ProxiCloud, wantsToPresentURL url: URL) {
        UIApplication.shared.open(url)
    }

    func proxiCloud(_ pc: ProxiCloud, didLoadGeofences geofences: [PCGeofenceProtocol]) {
        NotificationCenter.default.post(name: ProxiCloudManagerNotification.didLoadGeofences, object: nil)
    }

    func proxiCloud(_ pc: ProxiCloud, didChangeAuthorisationStatus status: CLAuthorizationStatus) {
        NotificationCenter.default.post(name: ProxiCloudManagerNotification.didChangeAuthorisationStatus, object: nil)
    }

    func proxiCloud(_ pc: ProxiCloud, didReportEvent event: PCEventProtocol) {
        eventsRepository.addReportedEvent(event)
        NotificationCenter.default.post(name: ProxiCloudManagerNotification.didRegisterNewEvent, object: nil)
    }

    func proxiCloud(_ pc: ProxiCloud, didRiseAnError error: Error) {
        let alertViewController = UIAlertController(
            title: "SDK Error",
            message: error.localizedDescription,
            preferredStyle: .alert
        )

        UIApplication.shared.topViewController?.present(
            alertViewController,
            animated: true
        )

        debugPrint("SDK Error: ", error.localizedDescription)
    }
}
