//
//  EventsViewModel.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 12/08/2022.
//

import Foundation
import ProxiCloudSDK

class EventsViewModel {
    private let proxiCloudManager: ProxiCloudManagerProtocol

    enum OutputAction {
        case reloadData
    }

    var performOutputAction: ((OutputAction) -> Void)?

    var rowsCount: Int {
        proxiCloudManager.reportedEvents.count
    }

    func event(for indexPath: IndexPath) -> PCEventProtocol? {
        proxiCloudManager.reportedEvents[safe: indexPath.row]
    }

    init(proxiCloudManager: ProxiCloudManagerProtocol = ProxiCloudManager.shared) {
        self.proxiCloudManager = proxiCloudManager

        observe()
    }

    private func observe() {
        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: ProxiCloudManagerNotification.didRegisterNewEvent, object: nil)
    }

    @objc private func reload() {
        self.performOutputAction?(.reloadData)
    }
}
