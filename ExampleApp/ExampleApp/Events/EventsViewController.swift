//
//  EventsViewController.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 12/08/2022.
//

import UIKit

class EventsViewController: UITableViewController {

    private let viewModel = EventsViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self

        viewModel.performOutputAction = { [weak self] action in
            switch action {
            case .reloadData:
                DispatchQueue.main.async {
                    self?.tableView.reloadData()
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowsCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventTableViewCell
        if let event = viewModel.event(for: indexPath) {
            cell.configure(with: event)
        }
        return cell
    }
}
