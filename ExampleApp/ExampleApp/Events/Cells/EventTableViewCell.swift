//
//  EventTableViewCell.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 30/08/2022.
//

import UIKit
import ProxiCloudSDK

class EventTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func configure(with event: PCEventProtocol) {
        titleLabel.text = event.action.content.body

        let description = [
            "Time: \(event.date)",
            "Transition: \(event.transition.name)",
            "Action UUID: \(event.action.uuid)"
        ].joined(separator: "\n")

        descriptionLabel.text = description
    }
}

