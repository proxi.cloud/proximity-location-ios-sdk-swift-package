//
//  UIApplication+TopViewController.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 29/01/2024.
//

import UIKit

extension UIApplication {
    var topViewController: UIViewController? {
        var topViewController: UIViewController? = nil
        if #available(iOS 13, *) {
            topViewController = self.connectedScenes.compactMap {
                return ($0 as? UIWindowScene)?.windows.filter { $0.isKeyWindow  }.first?.rootViewController
            }.first
        } else {
            topViewController = self.keyWindow?.rootViewController
        }
        return topViewController
    }
}
