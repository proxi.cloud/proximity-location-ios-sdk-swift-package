//
//  SettingsViewController.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 07/08/2022.
//

import UIKit
import AdSupport
import AppTrackingTransparency

class SettingsViewController: UIViewController {

    @IBOutlet weak var adIDLabel: UILabel!
    @IBOutlet weak var apiKeyLabel: UILabel!
    @IBOutlet weak var ibNotificationsButton: TutorialButton!
    @IBOutlet weak var ibLocationButton: TutorialButton!

    let asManager = ASIdentifierManager.shared()
    let proxiCloudManager = ProxiCloudManager.shared

    var adId: String {
        asManager.advertisingIdentifier.uuidString
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        adIDLabel.text = adId
        apiKeyLabel.text = proxiCloudManager.currentAPIKey
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        checkStatus()
    }

    @IBAction func getIDFA() {
        ATTrackingManager.requestTrackingAuthorization { [weak self] status in
            DispatchQueue.main.async {
                self?.handleTrackingRequestStatus()
            }
        }
    }

    @IBAction func changeAPIKey() {
        let alertVC = UIAlertController(title: "Change API key", message: nil, preferredStyle: .alert)

        alertVC.addTextField { textfield in
            textfield.placeholder = "Type new API key"
        }

        alertVC.addAction(UIAlertAction(title: "Save", style: .default, handler: { [weak self] _ in
            guard let apiKey = alertVC.textFields?.first?.text, !apiKey.isEmpty else { return }

            self?.handleAPIKeyChange(apiKey)
        }))

        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))

        present(alertVC, animated: true)
    }

    private func handleTrackingRequestStatus() {
        adIDLabel.text = adId

        Task {
           try await proxiCloudManager.setAdIdentifier(adId)
        }
    }

    private func handleAPIKeyChange(_ newAPIKey: String) {
        Task {
            try await proxiCloudManager.setApiKey(newAPIKey)
        }
        apiKeyLabel.text = proxiCloudManager.currentAPIKey
    }
}

extension SettingsViewController {
    @objc private func checkStatus() {
        let locationAlways = proxiCloudManager.locationAuthorizationStatus == .authorizedAlways
        ibLocationButton.status = locationAlways ? .granted : .denied

        proxiCloudManager.canReceiveNotifications { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let granted):
                    self?.ibNotificationsButton.status = granted ? .granted : .denied
                default:
                    break
                }
            }
        }
    }

    @IBAction func notificationsButtonClicked(_ sender: TutorialButton) {
        if sender.status != .granted {
            proxiCloudManager.requestNotificationsAuthorization {[weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let granted):
                        self?.ibNotificationsButton.status = granted ? .granted : .denied
                    default:
                        break
                    }
                }
            }
        }
    }

    @IBAction func locationButtonClicked(_ sender: UIButton) {
        if proxiCloudManager.locationAuthorizationStatus == .notDetermined  {
            proxiCloudManager.requestLocationAuthorization()
            return
        }

        if proxiCloudManager.locationAuthorizationStatus != .authorizedAlways {
            promptToChangeAutorization()
        }
    }

    private func promptToChangeAutorization() {
        let alertVC = UIAlertController(title: "Wrong Location Authorization Status", message: "Please change it to Awlays in settings", preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if let BUNDLE_IDENTIFIER = Bundle.main.bundleIdentifier,
                let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(BUNDLE_IDENTIFIER)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))

        present(alertVC, animated: true)
    }
}
