import UIKit

class TabBarController: UITabBarController {
    let proxiCloudManager = ProxiCloudManager.shared

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presentTutorialIfNeeded()
    }

    func presentTutorialIfNeeded() {
        if proxiCloudManager.shouldShowTutorial(){
            presentTutorial()
            return
        }

        proxiCloudManager.canReceiveNotifications { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let granted):
                    if !granted {
                        self?.presentTutorial()
                    }
                default:
                    break
                }
            }
        }
    }

    private func presentTutorial() {
        let vc: TutorialViewController = self.storyboard?.instantiateViewController(withIdentifier: "tutorial") as! TutorialViewController
        present(vc, animated: true)
    }
}
