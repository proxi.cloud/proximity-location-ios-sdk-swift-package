//
//  TriggersMapViewModel.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 08/08/2022.
//

import Foundation
import ProxiCloudSDK

class TriggersMapViewModel {
    private(set) var geofences: [PCGeofenceProtocol]

    init(geofences: [PCGeofenceProtocol]) {
        self.geofences = geofences
    }
}
