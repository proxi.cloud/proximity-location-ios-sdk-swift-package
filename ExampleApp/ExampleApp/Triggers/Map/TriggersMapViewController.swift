import UIKit
import MapKit
import ProxiCloudSDK

class TriggersMapViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!

    var geofences: [PCGeofenceProtocol] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        setupMap()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setupAnnotations()
    }

    private func setupMap() {
        mapView.delegate = self
        mapView.showsUserLocation = true
    }
    private func setupAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)

        geofences.forEach { geofence in
            let annotation = MKPointAnnotation()
            let loc = CLLocationCoordinate2D(latitude: geofence.latitude, longitude: geofence.longitude)
            annotation.coordinate = loc
            mapView.addAnnotation(annotation)

            let circle = MKCircle(center: loc, radius: geofence.radius)
            mapView.addOverlay(circle)
        }

        mapView.fitAll()
    }
}

extension TriggersMapViewController: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let circleView = MKCircleRenderer(overlay: overlay)
        circleView.lineWidth = 3
        circleView.strokeColor = .purple.withAlphaComponent(0.7)
        circleView.fillColor = .purple.withAlphaComponent(0.3)
        return circleView
    }
}


extension MKMapView {
    func fitAll() {
        var zoomRect = MKMapRect.null;
        for annotation in annotations {
            let annotationPoint = MKMapPoint(annotation.coordinate)
            let pointRect       = MKMapRect(x: annotationPoint.x, y: annotationPoint.y, width: 0.01, height: 0.01);
            zoomRect            = zoomRect.union(pointRect);
        }
        setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 100, left: 100, bottom: 100, right: 100), animated: true)
    }
}
