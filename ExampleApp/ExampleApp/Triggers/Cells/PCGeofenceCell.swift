import UIKit

class PCGeofenceCell: UITableViewCell {
    @IBOutlet weak var totalLabel: UILabel!

    func setTotalLabelText(_ text: String?) {
        totalLabel.text = text
    }
}
