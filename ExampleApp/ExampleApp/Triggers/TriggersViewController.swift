import UIKit

class TriggersViewController: UITableViewController {

    @IBOutlet weak var rippleView: UIRippleView!
    @IBOutlet weak var sonarContainer: UIView!
    @IBOutlet weak var empty: UILabel!

    private let viewModel = TriggersViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        bind()
        viewModel.start()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        reload()
    }

    private func bind() {
        viewModel.performOutputAction = { [weak self] action in
            switch action {
            case .reloadData:
                self?.reload()
            }
        }
    }

    private func reload() {
        DispatchQueue.main.async { [weak self] in
            self?.tableView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.totalCount
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "geofenceCell", for: indexPath) as! PCGeofenceCell
        cell.setTotalLabelText(viewModel.title(for: indexPath))
        return cell
    }

    @IBAction func gotToMapView() {
        performSegue(withIdentifier: "showMap", sender: nil)
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap", let vc = segue.destination as? TriggersMapViewController {
            vc.geofences = viewModel.geofences
        }
    }
}

class UIRippleView: UIView {
    @IBOutlet weak var iconView: UIImageView!
}
