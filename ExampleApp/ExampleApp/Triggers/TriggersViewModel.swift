import Foundation
import ProxiCloudSDK

protocol TriggersViewModelProtocol {
    var totalCount: Int { get }
    var performOutputAction: ((TriggersViewModel.OutputAction) -> Void)? { get set }

    func start()
}

class TriggersViewModel: TriggersViewModelProtocol {
    private let proxiCloudManager: ProxiCloudManagerProtocol

    enum OutputAction {
        case reloadData
    }

    var performOutputAction: ((OutputAction) -> Void)?

    var geofences: [PCGeofenceProtocol] {
        proxiCloudManager.geofences
    }

    var totalCount: Int {
        geofences.count
    }

    init(proxiCloudManager: ProxiCloudManagerProtocol = ProxiCloudManager.shared) {
        self.proxiCloudManager = proxiCloudManager

        NotificationCenter.default.addObserver(self, selector: #selector(reload), name: ProxiCloudManagerNotification.didLoadGeofences, object: nil)
    }

    @objc private func reload() {
        self.performOutputAction?(.reloadData)
    }

    func start() {
        proxiCloudManager.start()
    }

    func title(for indexPath: IndexPath) -> String? {
        guard let geofence = geofences[safe: indexPath.row] else { return nil }

        return "ID:\(geofence.identifier)\nLONG: \(geofence.longitude)\nLAT: \(geofence.latitude)\nRADIUS: \(geofence.radius)"
    }
}
