//
//  EventsRepository.swift
//  ExampleApp
//
//  Created by Jan Lipmann on 12/08/2022.
//

import Foundation
import ProxiCloudSDK

protocol EventsRepositoryProtocol {
    var reportedEvents: [PCEventProtocol]  { get }

    func addReportedEvent(_ event: PCEventProtocol)
}

class EventsRepository: EventsRepositoryProtocol {
    static let shared = EventsRepository()

    private(set) var reportedEvents: [PCEventProtocol] = []

    func addReportedEvent(_ event: PCEventProtocol) {
        reportedEvents.append(event)
    }
}
