import UIKit
import AdSupport
import ProxiCloudSDK
import AppTrackingTransparency

class TutorialViewController: UIViewController {

    @IBOutlet weak var ibNotificationsButton: TutorialButton!
    @IBOutlet weak var ibLocationButton: TutorialButton!
    @IBOutlet weak var adIdNButton: TutorialButton!
    @IBOutlet weak var permissionsView: UIView!
    @IBOutlet weak var permissionsViewBottomConstreint: NSLayoutConstraint!

    let proxiCloudManager = ProxiCloudManager.shared

    private var locationEnabled: Bool = false

    private var adId: String {
        ASIdentifierManager.shared().advertisingIdentifier.uuidString
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        checkStatus()

        NotificationCenter.default.addObserver(self, selector: #selector(checkStatus), name: ProxiCloudManagerNotification.didChangeAuthorisationStatus, object: nil)
    }

    @objc private func checkStatus() {
        let locationAlways = proxiCloudManager.locationAuthorizationStatus == .authorizedAlways
        ibLocationButton.status = locationAlways ? .granted : .denied
        adIdNButton.status = ATTrackingManager.trackingAuthorizationStatus == .authorized ? .granted : .denied

        proxiCloudManager.canReceiveNotifications { [weak self] result in
            DispatchQueue.main.async {
                switch result {
                case .success(let granted):
                    self?.ibNotificationsButton.status = granted ? .granted : .denied
                default:
                    break
                }
            }
        }
    }

    @IBAction func notificationsButtonClicked(_ sender: TutorialButton) {
        if sender.status != .granted {
            proxiCloudManager.requestNotificationsAuthorization {[weak self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .success(let granted):
                        self?.ibNotificationsButton.status = granted ? .granted : .denied
                    default:
                        break
                    }
                }
            }
        }
    }

    @IBAction func locationButtonClicked(_ sender: UIButton) {
        if proxiCloudManager.locationAuthorizationStatus == .notDetermined  {
            proxiCloudManager.requestLocationAuthorization()
            return
        }

        if proxiCloudManager.locationAuthorizationStatus != .authorizedAlways {
            promptToChangeAutorization()
        }
    }

    @IBAction func adIdButtonClicked(_ sender: UIButton) {
        ATTrackingManager.requestTrackingAuthorization { [weak self] status in
            DispatchQueue.main.async {
                self?.handleTrackingRequestStatus()
            }
        }
    }

    private func promptToChangeAutorization() {
        let alertVC = UIAlertController(title: "Wrong Location Authorization Status", message: "Please change it to Awlays in settings", preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        alertVC.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            if let BUNDLE_IDENTIFIER = Bundle.main.bundleIdentifier,
                let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(BUNDLE_IDENTIFIER)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }))

        present(alertVC, animated: true)
    }

    private func handleTrackingRequestStatus() {
        Task {
            try await proxiCloudManager.setAdIdentifier(adId)
            adIdNButton.status = ATTrackingManager.trackingAuthorizationStatus == .authorized ? .granted : .denied
        }
    }

    @IBAction func closeMe() {
        dismiss(animated: true)
    }
}

class TutorialButton: UIButton {
    enum Status {
        case granted
        case denied

        var icon: String {
            switch self {
            case .granted:
                return "icon_v"
            case .denied:
                return "icon_x"
            }
        }
    }

    var status: Status = .denied {
        didSet {
            setImage(UIImage(named: status.icon), for: .normal)
        }
    }
}
