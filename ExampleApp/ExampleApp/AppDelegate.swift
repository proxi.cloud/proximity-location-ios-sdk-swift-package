import UIKit
import UserNotifications
import ProxiCloudSDK
import FirebaseCore
import FirebaseCrashlytics
import FirebaseMessaging

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let proxiCloudManager = ProxiCloudManager.shared

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupFirebase()
        application.registerForRemoteNotifications()

        return true
    }

    private func setupFirebase() {
        UNUserNotificationCenter.current().delegate = self
        FirebaseApp.configure()

        Messaging.messaging().delegate = self
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().setAPNSToken(deviceToken, type: .unknown)
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        proxiCloudManager.applicationWillEnterForeground(application)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        proxiCloudManager.handleNotification(notification)
        completionHandler([.banner, .sound])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        proxiCloudManager.handleNotification(response.notification)
        completionHandler()
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        messaging.subscribe(toTopic: "proxicloud") { error in
            print("Subsribed to default proxi.cloud topic")
        }
    }
}

